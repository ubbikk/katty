import pathlib


def replace_in_string(s, a, b):
    return s.replace(a, b)


def replace_in_file(f, a, b):
    s = f.read_text(encoding='utf-8')
    s = replace_in_string(s, a, b)
    f.write_text(s, encoding='utf-8')


def replace_in_folder(folder, a, b):
    folder = pathlib.Path(folder)
    for p in folder.iterdir():
        if p.stem.startswith('.'):
            print(f'skiping zasranets: {p.stem}')
            continue
        print(p.stem)
        if p.is_file():
            replace_in_file(p, a, b)


if __name__ == '__main__':
    folder = '/home/dmytro/projects/katty'
    a = 'string you want to have'
    b = 'string you want to have'
    replace_in_folder(folder, a, b)
