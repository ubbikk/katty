def dima_lalala_function(Y=3, X=3, Z=0):
    s = '-'.join(['la'] * X)
    s = '\n'.join([s] * Y)
    if Z == 0:
        s += '.'
    else:
        s += '!'
    return s


print(dima_lalala_function(5, 7))
print('\n\n')
print(dima_lalala_function(5, 7, 1))
print('\n\n')
print(dima_lalala_function(7))
print('\n\n')
print(dima_lalala_function())
