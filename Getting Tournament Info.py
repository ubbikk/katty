import requests

url = "http://api-racing.golden-piggy.com/tournaments/"

headers = {
    'x-auth-token': "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxOCwiaXAiOiI1LjQ1Ljg1LjIwNiIsImV4cGlyZV90aW1lIjoxNTM4MDU1ODc3fQ.nKk8xFZYmt8JFSOcLVUyPG5ckP4WO7cHWuDAJec3Zws",
    'cache-control': "no-cache",
    'postman-token': "3ed016af-493d-0cb1-8666-4a72bae3030b"
    }

response = requests.request("GET", url, headers=headers)

print(response.text)